<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <title>Ajax scroll down pagination with php and mysql</title>

     <!-- Bootstrap -->
     <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
     <link href="bootstrap/css/bootstrap-social.css" rel="stylesheet">
     <link href="bootstrap/css/font-awesome.css" rel="stylesheet">
     <link href="css/style.css" rel="stylesheet">
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
      <script src="bootstrap/html5shiv.js"></script>
      <script src="bootstrap/respond.min.js"></script>
     <![endif]-->

     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="bootstrap/js/jquery-1.11.1.min.js"></script>
</head>
<body>
     <div class="container mainbody">
          <h1>Ajax infinite scroll down pagination with php and mysql</h1>
          <div class="center">
               <a class="btn btn-default" href="index.php">Auto Loading Content </a>
               <a class="btn btn-primary" href="demo2.php">Click Loading Content </a>
          </div>
          <div class="col-lg-9 col-lg-offset-3">
       
               <div class="col-lg-12" id="results"></div>
               <div id="loader_image"><img src="loader.gif" alt="" width="24" height="24"> Loading...please wait</div>
               <div id="loader_message"></div>
          </div>
    </div>

     <script type="text/javascript">
     var busy = false;
     var limit = 15
     var offset = 0;

     function displayRecords(lim, off) {
          $.ajax({
               type: "GET",
               async: false,
               url: "getrecords.php",
               data: "limit=" + lim + "&offset=" + off,
               cache: false,
               beforeSend: function() {
                    $("#loader_message").html("").hide();
                    $('#loader_image').show();
               },
               success: function(html) {
                    $("#results").append(html);
                    $('#loader_image').hide();
                    if (html == "") {
                         $("#loader_message").html('<button class="btn btn-default" type="button">No more records.</button>').show()
                    } else {
                         $("#loader_message").html('<button class="btn btn-default" type="button">Loading please wait...</button>').show();
                    }
                    window.busy = false;
               }
          });
     }

     $(document).ready(function() {
     // start to load the first set of data
          if (busy == false) {
               busy = true;
               // start to load the first set of data
               displayRecords(limit, offset);
          }


          $(window).scroll(function() {
               // make sure u give the container id of the data to be loaded in.
               if ($(window).scrollTop() + $(window).height() > $("#results").height() && !busy) {
                    busy = true;
                    offset = limit + offset;

                    // this is optional just to delay the loading of data
                    setTimeout(function() { displayRecords(limit, offset); }, 500);

                    // you can remove the above code and can use directly this function
                    // displayRecords(limit, offset);
               }
          });
     });
     </script>

     <!-- Include all compiled plugins (below), or include individual files as needed -->
     <script src="bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
