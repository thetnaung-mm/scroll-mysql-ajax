<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <title>Ajax scroll down pagination with php and mysql - thesoftwareguy</title>

     <!-- Bootstrap -->
     <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
     <link href="bootstrap/css/bootstrap-social.css" rel="stylesheet">
     <link href="bootstrap/css/font-awesome.css" rel="stylesheet">
     <link href="css/style.css" rel="stylesheet">
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
      <script src="bootstrap/html5shiv.js"></script>
      <script src="bootstrap/respond.min.js"></script>
     <![endif]-->

     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="bootstrap/js/jquery-1.11.1.min.js"></script>
</head>
<body>
     <div class="container mainbody">
          <h1>Ajax scroll down pagination with php and mysql</h1>
          
          <div class="center">
               <a class="btn btn-default" href="index.php">Auto Loading Content </a>
               <a class="btn btn-primary" href="demo2.php">Click Loading Content </a>
          </div>

          <div class="col-lg-9 col-lg-offset-2">
               <div class="col-lg-12" id="results"></div>
               <div id="loader_image"><img src="loader.gif" alt="" width="24" height="24"> Loading...please wait</div>
               <div id="loader_message"></div>
          </div>
     </div>


     <script type="text/javascript">
     var limit = 10
     var offset = 0;

     function displayRecords(lim, off) {
          $.ajax({
          type: "GET",
          async: false,
          url: "getrecords.php",
          data: "limit=" + lim + "&offset=" + off,
          cache: false,
          beforeSend: function() {
               $("#loader_message").html("").hide();
               $('#loader_image').show();
          },
          success: function(html) {
               $('#loader_image').hide();
               $("#results").append(html);

               if (html == "") {
                    $("#loader_message").html('<button data-atr="nodata" class="btn btn-default" type="button">No more records.</button>').show()
               } else {
                    $("#loader_message").html('<button class="btn btn-default" type="button">Load more data</button>').show();
               }
          }
     });
     }

     $(document).ready(function() {
          // start to load the first set of data
          displayRecords(limit, offset);

          $('#loader_message').click(function() {

               // if it has no more records no need to fire ajax request
               var d = $('#loader_message').find("button").attr("data-atr");
               if (d != "nodata") {
                    offset = limit + offset;
                    displayRecords(limit, offset);
               }
          });
     });
     </script>

     <!-- Include all compiled plugins (below), or include individual files as needed -->
     <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
